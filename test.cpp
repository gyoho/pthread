//====================================================================================
// Customer threads and Seller threads are working in parallel
// Within customer threads and seller threads,
// Multiple customers are working in parallel
// Multiple sellers are working in parallel
// 
// Upon created, threads are executing the function pointed by its address
// Within the function used as the thread entry point, define the its job
// call other functions, create variables or do anything any other function can do
// Each customer and seller are executing their entry function in the order defined
// 
// As they working in parallel, use mutex to protect critical region
// and use semaphore to enforce the order to be executed if in another kind of thread
// Also use print mutex to allow only one thread print at a time
// Otherwise, it gets really messy
//====================================================================================



#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>
#include <signal.h>
#include <queue>
#include <sys/time.h>
#include <iostream>

using namespace std;


// Global variables
#define NUM_sellerH 1
#define NUM_sellerM 3
#define NUM_sellerL 6

#define MAX_SELLING_DURATION_H 2
#define MAX_SELLING_DURATION_M 4
#define MAX_SELLING_DURATION_L 7

#define NUM_SEATS 10

#define TIME_DURATION 60

/*Semaphore should be global*/
pthread_mutex_t positionsMutex;  // mutex protects position
pthread_mutex_t seatsMutex;      // mutex protects seats
pthread_mutex_t printMutex;      // mutex protect printing simultaneously
sem_t lineUp;                    // seller waits on this semaphore


struct itimerval sellingTimer;   // Ticket selling time
time_t startTime;                // time_t, time object
int timesUp = 0;  // 1 = selling time is over


int seats[NUM_SEATS][NUM_SEATS];
queue<int> waitQueue;            // waiting queue for customers
queue<int> leaveQueue;           // Keep track of left customers


int customer_id;
int seat_no;


int firstPrint = 1;


//   Print a line for each event:
//   elapsed time
//   A customer arrives at the tail of a seller’s queue
//   A customer is served and is assigned a seat, or is told the concert is sold out
//   A customer completes a ticket purchase and leaves.

void print(char *event)
{
    // struct time declaraion
    time_t now;

    // Get the current time as a value of type time_t.
    time(&now);
    
    // double difftime(time_t time1, time_t time2)
    // returns the difference of seconds between time1 and time2
    double elapsed = difftime(now, startTime);
    
    int min = 0;
    int sec = (int) elapsed;

    if (sec >= 60) {
        min++;
        sec -= 60;
    }

    // Acquire the mutex lock to protect the printing.
    pthread_mutex_lock(&printMutex);

    if (firstPrint) {
        cout << "TIME | ARRIVEING | SERVING  | EVENT\n";
        firstPrint = 0;
    }

    // Elapsed time.
    printf("%1d:%2d | ", min, sec);

    // Who's meeting with the professor.
    if (customer_id > 0) {
        printf("%5d   |", customer_id);
    }
    else {
        printf("        |");
    }

    /*int k = 0;
    // What event occurred.
    while (k++ < CHAIR_COUNT) printf("    ");
    printf(" | %s\n", event);*/

    // Release the mutex lock.
    pthread_mutex_unlock(&printMutex);
}


// A customer arrives.
void customerArrives(int cus_id)
{
    // cout << endl << "enter customer" << endl;
    // Customer is served
    // Enter the queue
   
    // Get the mutex lock to protect the queue
    pthread_mutex_lock(&positionsMutex);

    // Critical region: get a position in a queue
    // This is FCFS
    cout << "A customer#" << cus_id << " enter the queue" << endl;
    waitQueue.push(cus_id);

    // Release the mutex lock.
    pthread_mutex_unlock(&positionsMutex);

    // Signal the "lineUp" semaphore
    // To start get a ticket process
    cout << "signal semaphore lineUp" << endl;
    sem_post(&lineUp);  // signal

    /*Jump to sem_wait(&lineUp) in sellerServeCustomer*/
}

// The customer thread
// Taking a pointer to void as a parameter and returning a pointer to void
void *customer(void *param)
{
    int cus_id = *((int *) param);
    // Customers will arrive at random times during an hour.
    sleep(rand()%TIME_DURATION);

    if(!timesUp)
        customerArrives(cus_id);

    return NULL;
}

int findSeatL()
{
    cout << "Finding seat" << endl;

    seat_no = 0;

    // Look for available seats
    for(int i=NUM_SEATS - 1; i>0; i--){
        for(int j=0; j<NUM_SEATS; j++){
            // If find an available seat
            if(seats[i][j] == 0){
                // Return the seat #
                return seat_no = i*10 + j;
            }
        }
    }

    // If no seat found, return 0
    cout << "Not found seat" << endl;
    cout << "Seat#" << seat_no << endl;
    return seat_no;
}

void getSeat(int customer_id, int seat_no)
{
    // Keep out ohter processes
    /*sem_wait(&rw_sem);
    cout << "semaphore jump to here" << endl;*/


    // If no more tickets
    if(seat_no == 0) {
        cout << "No seat found" << endl;
        leaveQueue.push(customer_id);
        char event[80];
        sprintf(event, "Customer %d leaves", customer_id);
        print(event);
    }


    // There is an available seat
    else{       
        // Protect the seats
        //pthread_mutex_lock(&seatsMutex);

        // Sell the seat
        // Set the customer # to the seat
        cout << "Selling the seat#" << seat_no << " to customer#"
             << customer_id << endl;
        seats[seat_no / 10][seat_no % 10] = customer_id;

        // Release the mutex lock.
        //pthread_mutex_unlock(&seatsMutex);

        // Let other process in
        //sem_post(&rw_sem);
    }
}

// Seller sells a ticket or waiting for next customer
void sellerServeCustomer(int seller_id)
{
    cout << "Seller #" << seller_id << " starts selling" << endl;

    // No customer in the queue, so wait 
    if (waitQueue.size() == 0) {
        // Do nothing
        cout << "no customer" << endl;
    }

    if (!timesUp) {
    /*Wait for customer's lineing up*/
        cout << "Wait on the queue semaphore lineUp" << endl;
        sem_wait(&lineUp);
        cout << "Get the signal of semaphore lineUp" << endl;

    /*Serve a customer*/
        // Get the mutex lock to protect the queue
        pthread_mutex_lock(&positionsMutex);
        cout << "Locked up the pos mutex" << endl;
        // Get the customer id
        customer_id = 0;
        customer_id = waitQueue.front();
        cout << "get customers id" << endl;
        cout << "Seller #" << seller_id << " serves " << "Customer#" << customer_id << endl;

        // Get the mutex lock to protect the seats
        pthread_mutex_lock(&seatsMutex);
        cout << "locked up seat mutex" << endl;


        // Look for an available seat
        cout << "Try to find seat" << endl;
        seat_no = 0; 
        seat_no = findSeatL();
        cout << "Seller#? --> " << seller_id << endl;
        cout << "Seat#? --> " << seat_no << endl;
        cout << "Customer#? --> " << customer_id << endl;
        getSeat(customer_id, seat_no);

        // If no more tickets
        /*if(seat_no == 0) {
            cout << "No seat found" << endl;
            leaveQueue.push(customer_id);
            char event[80];
            sprintf(event, "Customer %d leaves", customer_id);
            print(event);
        }*/

        // Otherwise, sell the seat to the customer
       /* else{
            cout << "Try to get seat" << endl;
            getSeat(seat_no, customer_id);
        }*/

        // Release the mutex lock.
        pthread_mutex_unlock(&seatsMutex);
        cout << "Released seat mutex" << endl;

        // Critical region: remove a customer from a queue
        // This is FCFS, service the customer in the front
        leaveQueue.push(customer_id);
        waitQueue.pop();
        cout << "customer#" << leaveQueue.back() << " left" << endl;
        //cout << "Clean the queue" << endl;

        // Release the mutex lock.
        pthread_mutex_unlock(&positionsMutex);
        cout << "released pos mutex" << endl;

        char event[80];
        sprintf(event, "Seller serves the customer %d", customer_id);
        print(event);

        // Seller sells ticket in appropriate random time
        sleep(rand()%MAX_SELLING_DURATION_H + 1);

        /*sem_post(&lineUp);
        cout << "Released semaphore" << endl;*/

        sprintf(event, "Seller finishes with customer %d", customer_id);
        customer_id = 0;
        print(event);
    }
}

/*int findSeatH(int seats_array[][NUM_SEATS], int array_size)
{
    int seat_no = 0;

    // Keep out ohter writers
    sem_wait(&rw_sem);
   
    // Look for available seats
    for(int i=0; i<array_size; i++){
        for(int j=0; j<NUM_SEATS; j++){
            // If find an available seat
            if(array_seats[i][j] == 0){
                // Return the seat #
                return seat_no = i*10 + j;
            }
        }
    }

    // Let other process in
    sem_post(&rw_sem);

    // If no seat found, return 0
    return seat_no;
}*/


// The professor thread
// Taking a pointer to void as a parameter and returning a pointer to void
void *seller(void *param)
{
    int seller_id = *((int *) param);
    cout << "Seller #" << seller_id << " starts its thread" << endl;

    // Time starts when seller opens their windows 
    time(&startTime);
    

    // Data Type: struct itimerval
    /*struct itimerval {
      struct timeval it_interval;  // next value 
      struct timeval it_value;     // current value 
    };*/
    /*typedef struct timeval {
      long tv_sec;  // Time interval, in seconds
      long tv_usec; // Time interval, in microseconds    
    } timeval;*/

    // Set timeval !!wall-clock time!! for an hour duration
    sellingTimer.it_value.tv_sec = TIME_DURATION;


    // The setitimer function sets the timer specified by which according to new.
    /***Function: int setitimer (int which, const struct itimerval *new, struct itimerval *old)***/
    // which: the timer code, specifying which timer to set.
    // The timer code is ITIMER_REAL, the process is sent a SIGALRM signal
    // after the specified !!wall-clock time!! has elapsed (60secs)
    setitimer(ITIMER_REAL, &sellingTimer, NULL);

    // cout << "Enter sellerServerCustomer function" << endl;

    // Sell tickets until selling time is over.
    do {
        sellerServeCustomer(seller_id);
    } while (!timesUp);

    cout << "Seller closes their windows" << endl;
    return NULL;
}


void printSeatsChart() {
    for(int i=0; i<NUM_SEATS; i++){
        for(int j=0; j<NUM_SEATS; j++){
            // Modification needed!
            cout << seats[i][j];             
        }
    }
}

// Timer signal handler.
// Used with signal function
// When time's out, set timesUp to 1
void timerHandler(int signal) {
    timesUp = 1;  // Selling time is over
}

// Main.
int main(int argc, char *argv[])
{
    int N = 0;
    if (argc > 1){
        N = atoi(argv[1]);
    }
    else{
        cout<<"Please give an argument to the program.";
        return 1;
    }

    int customerIds[N];
    int sellerHId;
    int sellerMIds[3];
    int sellerLIds[6];

    // Initialize seats
    for(int i=0; i<NUM_SEATS; i++){
        for(int j=0; j<NUM_SEATS; j++){
            seats[i][j] = 0;
        }
    }
    
    // Initialize the mutexes and the semaphore.
    pthread_mutex_init(&positionsMutex, NULL);
    pthread_mutex_init(&seatsMutex, NULL);
    pthread_mutex_init(&printMutex, NULL);


    // Passing the value of one would enable the semaphore to be shared between multiple processes
    // Initialized a semaphore with a count of 0
    //sem_init(&filledSeats, 1, 0);

    srand(time(0));

    // Create the seller threads.
    /*for (int i = 0; i < num_sellerH; i++) {
        sellerHIds[i] = i;
        pthread_t sellerHThreadId;
        pthread_attr_t sellerHAttr;
        pthread_attr_init(&sellerHAttr);
        pthread_create(&sellerHThreadId, &sellerHAttr, sellerH, &sellerHIds[i]);
    }
    
    for (int i = 0; i < num_sellerM; i++) {
        sellerMIds[i] = i;
        pthread_t sellerMThreadId;
        pthread_attr_t sellerMAttr;
        pthread_attr_init(&sellerMAttr);
        pthread_create(&sellerMThreadId, &sellerMAttr, sellerM, &sellerMIds[i]);
    }*/

    pthread_t sellerLThreadId;
    pthread_attr_t sellerLAttr;
    pthread_attr_init(&sellerLAttr);

    //cout << "about to create sellers" << endl;

    for (int i = 1; i <= NUM_sellerL; i++) {
        sellerLIds[i] = i;
        pthread_create(&sellerLThreadId, &sellerLAttr, seller, &sellerLIds[i]);
    }

    //cout << "about to create customers" << endl;

    // Create the customer threads.
    for (int i = 1; i <= N; i++) {
        customerIds[i] = i;
        pthread_t customerThreadId;
        pthread_attr_t customerAttr;
        pthread_attr_init(&customerAttr);
        pthread_create(&customerThreadId, &customerAttr, customer, &customerIds[i]);
    }
    

    // Send alarm when the time's out
    /*cout << "Times Up" << endl;*/
    signal(SIGALRM, timerHandler);

    // Wait for the sellers to complete the office hour.
    /*pthread_join(sellerHThreadId, NULL);
    pthread_join(sellerMThreadId, NULL);*/
    pthread_join(sellerLThreadId, NULL);
    cout << "Finishing the last thread" << endl;
    
    // Remaining waiting customers leave.
    cout << "All customers leave" << endl;
    while (waitQueue.size() > 0) {
        leaveQueue.push(waitQueue.front());
        waitQueue.pop();
        
        char event[80];
        sprintf(event, "Customer %d leaves",  customer_id);
        print(event);
    }

    int leftCount = leaveQueue.size();
    int ervedCount = N - leftCount;

    cout << "End of the program" << endl;

    return 0;
}
