// Main.
int main(int argc, char *argv[])
{
    int N = atoi( argv[1] );
    int customerIds[N];
    int sellerHId;
    int sellerMIds[3];
    int sellerLIds[6];

    // Initialize seats
    for(int i=0; i<NUM_SEATS; i++){
        for(int j=0; j<NUM_SEATS; j++){
            seats[i][j] = 0;
        }
    }
    
    // Initialize the mutexes and the semaphore.
    pthread_mutex_init(&seatMutex, NULL);
    pthread_mutex_init(&printMutex, NULL);


    // Passing the value of one would enable the semaphore to be shared between multiple processes
    // Initialized a semaphore with a count of 0
    sem_init(&filledSeats, 1, 0);

    srand(time(0));

    // Create the seller threads.
    for (int i = 0; i < num_sellerH; i++) {
        sellerHIds[i] = i;
        pthread_t sellerHThreadId;
        pthread_attr_t sellerHAttr;
        pthread_attr_init(&sellerHAttr);
        pthread_create(&sellerHThreadId, &sellerHAttr, sellerH, &sellerHIds[i]);
    }
    
    for (int i = 0; i < num_sellerM; i++) {
        sellerMIds[i] = i;
        pthread_t sellerMThreadId;
        pthread_attr_t sellerMAttr;
        pthread_attr_init(&sellerMAttr);
        pthread_create(&sellerMThreadId, &sellerMAttr, sellerM, &sellerMIds[i]);
    }

    for (int i = 0; i < sellerL; i++) {
        sellerLIds[i] = i;
        pthread_t sellerLThreadId;
        pthread_attr_t sellerLAttr;
        pthread_attr_init(&selleLMAttr);
        pthread_create(&sellerLThreadId, &sellerLAttr, sellerL, &sellerLIds[i]);
    }

    // Create the customer threads.
    for (int i = 0; i < N; i++) {
        customerIds[i] = i;
        pthread_t customerThreadId;
        pthread_attr_t customerAttr;
        pthread_attr_init(&customerAttr);
        pthread_create(&customerThreadId, &customerAttr, customer, &customerIds[i]);
    }
    

    // Send alarm when the time's out
    signal(SIGALRM, timerHandler);

    // Wait for the sellers to complete the office hour.
    pthread_join(sellerHThreadId, NULL);
    pthread_join(sellerMThreadId, NULL);
    pthread_join(sellerLThreadId, NULL);
    
    // Remaining waiting customers leave.
    while (waitQueue.size()-- > 0) {
        leaveQueue.push(waitQueue.front());
        waitQueue.pop();
        
        char event[80];
        sprintf(event, "Customer %d leaves",  studentId);
        print(event);
    }

    int leftCount = leaveQueue.size();
    int ervedCount = N - leftCount;

    return 0;
}
