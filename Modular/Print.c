void printSeatsChart()
{
    for(int i=0; i<array_size; i++){
        for(int j=0; j<NUM_SEATS; j++){
            // Modification needed!
            cout << seats_array[i][j];             
    }
}

int firstPrint = 1;


//   Print a line for each event:
//   elapsed time
//   A customer arrives at the tail of a seller’s queue
//   A customer is served and is assigned a seat, or is told the concert is sold out
//   A customer completes a ticket purchase and leaves.

void print(char *event)
{
    // struct time declaraion
    time_t now;

    // Get the current time as a value of type time_t.
    time(&now);
    
    // double difftime(time_t time1, time_t time2)
    // returns the difference of seconds between time1 and time2
    double elapsed = difftime(now, startTime);
    
    int min = 0;
    int sec = (int) elapsed;

    if (sec >= 60) {
        min++;
        sec -= 60;
    }

    // Acquire the mutex lock to protect the printing.
    pthread_mutex_lock(&printMutex);

    if (firstPrint) {
        printf("TIME | ARRIVEING | SERVING  | EVENT\n");
        firstPrint = 0;
    }

    // Elapsed time.
    printf("%1d:%2d | ", min, sec);

    // Who's meeting with the professor.
    if (meetingId > 0) {
        printf("%5d   |", meetingId);
    }
    else {
        printf("        |");
    }

    int k = 0;
    // What event occurred.
    while (k++ < CHAIR_COUNT) printf("    ");
    printf(" | %s\n", event);

    // Release the mutex lock.
    pthread_mutex_unlock(&printMutex);
}
