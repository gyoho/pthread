// Seller sells a ticket or waiting for next customer
void sellerServeCustomer()
{
    // No customer in the queue, so wait 
    if (waitQueue.size() == 0) {
        // Do nothing
    }

    if (!timesUp) {
    /*Wait for customer's lineing up*/
        // Wait on the "queue" semaphore
        sem_wait(&lineUp);

    /*Serve a customer*/
        // Get the mutex lock to protect the queue
        pthread_mutex_lock(&positionsMutex);
        // Get the customer id
        int customer_id = waitQueue.front();

        // Get the mutex lock to protect the seats
        pthread_mutex_lock(&seatsMutex);

        // Look for an available seat
        int seat_no = findSeatH();

        // If no more tickets
        if(seat_no == 0) {
            leaveQueue.push(customer_id);
            char event[80];
            sprintf(event, "Customer %d leaves", customer_id);
            print(event);
        }

        // Otherwise, sell the seat to the customer
        else{
            getSeat(seat_no, customer_id)
        }

        // Release the mutex lock.
        pthread_mutex_unlock(&seatsMutex);

        // Critical region: remove a customer from a queue
        // This is FCFS, service the customer in the front
        leaveQueue.push(customer_id);
        waitQueue.pop();

        // Release the mutex lock.
        pthread_mutex_unlock(&positionsMutex);

        char event[80];
        sprintf(event, "Seller serves the customer %d", customer_id);
        print(event);

        // Seller sells ticket in appropriate random time
        sleep(rand()%MAX_SELLING_DURATION_H + 1);

        sprintf(event, "Seller finishes with customer %d", customer_id);
        customer_id = 0;
        print(event);
    }
}


int findSeatH()
{
    cout << "Finding seat" << endl;

    seat_no = 0;

    // Look for available seats
    for(int i=0; i<NUM_SEATS; i++){
        for(int j=0; j<NUM_SEATS; j++){
            // If find an available seat
            if(seats[i][j] == 0){
                // Return the seat #
                return seat_no = i*10 + j;
            }
        }
    }

    // If no seat found, return 0
    cout << "Not found seat" << endl;
    cout << "Seat#" << seat_no << endl;
    return seat_no;
}

int findSeatM()
{
    cout << "Finding seat" << endl;

    seat_no = 0;
    int pos = 5;

    // Look for available seats
    for(int i=0; i<NUM_SEATS; i++){
        // add odd number and substract even numbr
        if(i%2 == 1)
            pos += i;
        else
            pos -= i;

        for(int j=0; j<NUM_SEATS; j++){
            // If find an available seat
            if(seats[pos][j] == 0){
                // Return the seat #
                return seat_no = i*10 + j;
            }
        }
    }

    // If no seat found, return 0
    cout << "Not found seat" << endl;
    cout << "Seat#" << seat_no << endl;
    return seat_no;
}

int findSeatL()
{
    cout << "Finding seat" << endl;

    seat_no = 0;

    // Look for available seats
    for(int i=NUM_SEATS - 1; i>0; i--){
        for(int j=0; j<NUM_SEATS; j++){
            // If find an available seat
            if(seats[i][j] == 0){
                // Return the seat #
                return seat_no = i*10 + j;
            }
        }
    }

    // If no seat found, return 0
    cout << "Not found seat" << endl;
    cout << "Seat#" << seat_no << endl;
    return seat_no;
}


void getSeat(int seat_no,  int customer_id){
    // There is an available seat
    if(seat_no != 0){

        // Keep out ohter processes
        sem_wait(&rw_sem);
        
        // Protect the seats
        //pthread_mutex_lock(&seatsMutex);

        // Sell the seat
        // Set the customer # to the seat
        seats_array[seat_no / 10][seat_no % 10] = customer_id;

        // Release the mutex lock.
        //pthread_mutex_unlock(&seatsMutex);

        // Let other process in
        sem_post(&rw_sem);
    }
}

// The professor thread.
void *seller(void *param)
{
    // Time starts when seller opens their windows 
    time(&startTime);
    print("Start selling tickts");

    // Data Type: struct itimerval
    /*struct itimerval {
      struct timeval it_interval;  // next value 
      struct timeval it_value;     // current value 
    };*/
    /*typedef struct timeval {
      long tv_sec;  // Time interval, in seconds
      long tv_usec; // Time interval, in microseconds    
    } timeval;*/

    // Set timeval !!wall-clock time!! for an hour duration
    sellingTimer.it_value.tv_sec = TIME_DURATION;


    // The setitimer function sets the timer specified by which according to new.
    /***Function: int setitimer (int which, const struct itimerval *new, struct itimerval *old)***/
    // which: the timer code, specifying which timer to set.
    // The timer code is ITIMER_REAL, the process is sent a SIGALRM signal
    // after the specified !!wall-clock time!! has elapsed (60secs)
    setitimer(ITIMER_REAL, &sellingTimer, NULL);

    // Sell tickets until selling time is over.
    do {
        sellerServeCustomer();
    } while (!timesUp);

    print("Seller closes their windows");
    return NULL;
}
