// Global variables

#define NUM_sellerH 1
#define NUM_sellerM 3
#define NUM_sellerL 6

#define MAX_SELLING_DURATION_H 2
#define MAX_SELLING_DURATION_M 4
#define MAX_SELLING_DURATION_L 7

#define NUM_SEATS 10

#define TIME_DURATION 60

/*Semaphore should be global*/
pthread_mutex_t positionsMutex;  // mutex protects position
pthread_mutex_t seatsMutex;      // mutex protects seats
sem_t lineUp = 1;                // seller waits on this semaphore
sem_t rw_sem = 1;                // finder-getter seat semaphore

struct itimerval sellingTimer;   // Ticket selling time
time_t startTime;                // time_t, time object

int seats[NUM_SEATS][NUM_SEATS];
queue<int> waitQueue;            // waiting queue for customers
queue<int> leaveQueue;           // Keep track of left customers