int timesUp = 0;  // 1 = selling time is over

// Timer signal handler.
// Used with signal function
// When time's out, set timesUp to 1
void timerHandler(int signal) {
    timesUp = 1;  // Selling time is over
}