// A customer arrives

// Similar to sellerID, group customerID by seller's type


void customerArrives(int cus_id)
{
    // cout << endl << "enter customer" << endl;
    // Customer is served
    // Enter the queue
   
    // Get the mutex lock to protect the queue
    pthread_mutex_lock(&positionsMutex);

    // Critical region: get a position in a queue
    // This is FCFS
    cout << "A customer#" << cus_id << " enter the queue" << endl;
    waitQueue.push(cus_id);

    // Release the mutex lock.
    pthread_mutex_unlock(&positionsMutex);

    // Signal the "lineUp" semaphore
    // To start get a ticket process
    cout << "signal semaphore lineUp" << endl;
    sem_post(&lineUp);  // signal

    /*Jump to sem_wait(&lineUp) in sellerServeCustomer*/
}

// The customer thread
// Taking a pointer to void as a parameter and returning a pointer to void
void *customer(void *param)
{
    int cus_id = *((int *) param);
    // Customers will arrive at random times during an hour.
    sleep(rand()%TIME_DURATION);

    if(!timesUp)
        customerArrives(cus_id);

    return NULL;
}