//====================================================================================
// Customer threads and Seller threads are working in parallel
// Within customer threads and seller threads,
// Multiple customers are working in parallel
// Multiple sellers are working in parallel
// 
// Upon created, threads are executing the function pointed by its address
// Within the function used as the thread entry point, define the its job
// call other functions, create variables or do anything any other function can do
// Each customer and seller are executing their entry function in the order defined
// 
// As they working in parallel, use mutex to protect critical region
// and use semaphore to enforce the order to be executed if in another kind of thread
// Also use print mutex to allow only one thread print at a time
// Otherwise, it gets really messy
//====================================================================================



#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>
#include <signal.h>
#include <queue>
#include <deque>
#include <sys/time.h>
#include <iostream>
#include <string>

using namespace std;


// Global variables
#define NUM_SELLERH 1
#define NUM_SELLERM 3
#define NUM_SELLERL 6

#define MAX_SELLING_DURATION_H 2
#define MAX_SELLING_DURATION_M 4
#define MAX_SELLING_DURATION_L 7

#define NUM_SEATS 10

#define TIME_DURATION 60

/*Semaphore should be global*/
pthread_mutex_t positionsMutex;  // mutex protects position
pthread_mutex_t seatsMutex;      // mutex protects seats
pthread_mutex_t printMutex;      // mutex protect printing simultaneously
pthread_mutex_t ticketMutex;     // mutex protect modifying ticket simultaneously
sem_t lineUp;                    // seller waits on this semaphore


struct itimerval sellingTimer;   // Ticket selling time
time_t startTime;                // time_t, time object
int timesUp = 0;  // 1 = selling time is over
int tickets = 100;


string seats[NUM_SEATS][NUM_SEATS];
// Keep track of the # of customer
// Each seller has alredy serviced
int num_custm_served[NUM_SELLERH + NUM_SELLERM + NUM_SELLERL];
deque<int> waitQueue;            // waiting queue for customers
queue<int> leaveQueueH;          // Keep track of left customers
queue<int> leaveQueueM;
queue<int> leaveQueueL;
queue<int> leaveQueue;

int customer_id;
int seat_no;

// To pass multiple argument to pthread
struct arg_struct {
    int sellerIds;
    int cusNum;
};

int firstPrint = 1;


//   Print a line for each event:
//   elapsed time
//   A customer arrives at the tail of a seller’s queue
//   A customer is served and is assigned a seat, or is told the concert is sold out
//   A customer completes a ticket purchase and leaves.

void print(char *event)
{
    // struct time declaraion
    time_t now;

    // Get the current time as a value of type time_t.
    time(&now);
    
    // double difftime(time_t time1, time_t time2)
    // returns the difference of seconds between time1 and time2
    double elapsed = difftime(now, startTime);
    
    int min = 0;
    int sec = (int) elapsed;

    if (sec >= 60) {
        min++;
        sec -= 60;
    }

    // Acquire the mutex lock to protect the printing.
    pthread_mutex_lock(&printMutex);

    if (firstPrint) {
        cout << "TIME | WAITING | EVENT\n";
        firstPrint = 0;
    }

    // Elapsed time.
    printf("%1d:%02d | ", min, sec);

    // Who's waiting the queue
    if(waitQueue.size() != 0){
        for(int i=0; i<waitQueue.size(); i++){
                printf("%5d", waitQueue.at(i));
        }
    }else{
        printf("     ");
    }


    //What event occurred.
    printf(" | %s\n", event);

    // Release the mutex lock.
    pthread_mutex_unlock(&printMutex);
}

void printSeatsChart() {
    for(int i=0; i<NUM_SEATS; i++){
        for(int j=0; j<NUM_SEATS; j++){
            // Modification needed!
            cout << seats[i][j];             
        }
    }
}


// A customer arrives.
void customerArrives(int cus_id)
{
    // cout << endl << "enter customer" << endl;
    // Customer is served
    // Enter the queue
   
    // Get the mutex lock to protect the queue
    pthread_mutex_lock(&positionsMutex);

    // Critical region: get a position in a queue
    // This is FCFS
    //cout << "A customer#" << cus_id << " enter the queue" << endl;
    waitQueue.push_back(cus_id);

    // Release the mutex lock.
    pthread_mutex_unlock(&positionsMutex);

    // Signal the "lineUp" semaphore
    // To start get a ticket process
    //cout << "signal semaphore lineUp" << endl;
    sem_post(&lineUp);  // signal

    /*Jump to sem_wait(&lineUp) in sellerServeCustomer*/
}

// The customer thread
// Taking a pointer to void as a parameter and returning a pointer to void
void *customer(void *param)
{
    int cus_id = *((int *) param);
    // Customers will arrive at random times during an hour.
    sleep(rand()%TIME_DURATION);

    if(!timesUp)
        customerArrives(cus_id);

    return NULL;
}

int findSeatH()
{
    //cout << "Finding seat H" << endl;

    seat_no = -1;

    // Look for available seats
    for(int i=0; i<NUM_SEATS; i++){
        for(int j=0; j<NUM_SEATS; j++){
            // If find an available seat
            if(seats[i][j] == "-"){
                // Return the seat #
                return seat_no = i*10 + j;
            }
        }
    }

    // If no seat found, return 0
    /*cout << "Not found seat" << endl;
    cout << "Seat#" << seat_no << endl;*/
    return seat_no;
}

int findSeatM()
{
    //cout << "Finding seat M" << endl;

    seat_no = -1;
    int pos = 5;

    // Look for available seats
    for(int i=0; i<NUM_SEATS; i++){
        // add odd number and substract even numbr
        if(i%2 == 1){
            pos += i;
        }
        else{
            pos -= i;
        }

        for(int j=0; j<NUM_SEATS; j++){
            // If find an available seat
            if(seats[pos][j] == "-"){
                // Return the seat #
                return seat_no = pos*10 + j;
            }
        }
    }

    // If no seat found, return 0
    /*cout << "Not found seat" << endl;
    cout << "Seat#" << seat_no << endl;*/
    return seat_no;
}

int findSeatL()
{
    //cout << "Finding seat L" << endl;

    seat_no = -1;

    // Look for available seats
    for(int i=NUM_SEATS - 1; i>0; i--){
        for(int j=0; j<NUM_SEATS; j++){
            // If find an available seat
            if(seats[i][j] == "-"){
                // Return the seat #
                return seat_no = i*10 + j;
            }
        }
    }

    // If no seat found, return 0
    /*cout << "Not found seat" << endl;
    cout << "Seat#" << seat_no << endl;*/
    return seat_no;
}


void getSeat(int seller_id, int customer_id, int seat_no)
{
    // Keep out ohter processes
    /*sem_wait(&rw_sem);
    cout << "semaphore jump to here" << endl;*/


    // If no more tickets
    if(seat_no == -1) {
        //cout << "No seat found" << endl;

        if (seller_id < NUM_SELLERH)
            leaveQueueH.push(customer_id);
        else if (seller_id < NUM_SELLERH + NUM_SELLERM)
            leaveQueueM.push(customer_id);
        else
            leaveQueueL.push(customer_id);

        char event[80];
        sprintf(event, "No seat. Customer %d leaves", customer_id);
        print(event);
    }


    // There is an available seat
    else{       
        // Protect the seats
        //pthread_mutex_lock(&seatsMutex);

        // Sell the seat
        // Set the customer # to the seat
        cout << "Selling the seat#" << seat_no << " to customer#" << customer_id << endl;
        //seats[seat_no / 10][seat_no % 10] = customer_id;

        if (seller_id < NUM_SELLERH){
            seats[seat_no / 10][seat_no % 10].assign("H0" + customer_id);
        }
        else if (seller_id < NUM_SELLERH + NUM_SELLERM){            
            seats[seat_no / 10][seat_no % 10].assign("M" + (seller_id - NUM_SELLERH - NUM_SELLERM + 1) + customer_id);
        }
        else{
            seats[seat_no / 10][seat_no % 10].assign("L" + (seller_id - NUM_SELLERH - NUM_SELLERM - NUM_SELLERL + 1) + customer_id);
        }

        // About to change the ticket
        pthread_mutex_lock(&ticketMutex);
        tickets--;
        pthread_mutex_unlock(&ticketMutex);

        /*pthread_mutex_lock(&printMutex);
        printSeatsChart();
        pthread_mutex_unlock(&printMutex);*/
    }
}

// Seller sells a ticket or waiting for next customer
void sellerServeCustomer(int seller_id)
{
    //cout << "Seller #" << seller_id << " starts selling" << endl;

    // No customer in the queue, so wait 
    if (waitQueue.size() == 0) {
        // Do nothing
        //cout << "no customer" << endl;
    }

    if (!timesUp) {
    /*Wait for customer's lineing up*/
        //cout << "Wait on the queue semaphore lineUp" << endl;
        sem_wait(&lineUp);
        //cout << "Get the signal of semaphore lineUp" << endl;

    /*Serve a customer*/
        // Get the mutex lock to protect the queue
        pthread_mutex_lock(&positionsMutex);
        //cout << "Locked up the pos mutex" << endl;
        // Get the customer id
        customer_id = waitQueue.front();
        //cout << "get customers id" << endl;
        //cout << "Seller#" << seller_id << " serves " << "Customer#" << customer_id << endl;
        char event[80];
        sprintf(event, "Seller#%d serves the customer %d", seller_id, customer_id);
        print(event);

        // Get the mutex lock to protect the seats
        pthread_mutex_lock(&seatsMutex);
        //cout << "locked up seat mutex" << endl;


        // Look for an available seat
        //cout << "Try to find seat" << endl;
        seat_no = 0; 


        // Different seller sells dieerent row of seats
        if (seller_id < NUM_SELLERH)
            seat_no = findSeatH();
        else if (seller_id < NUM_SELLERH + NUM_SELLERM)
            seat_no = findSeatM();
        else
            seat_no = findSeatL();


        /*cout << "Seller#? --> " << seller_id << endl;
        cout << "Seat#? --> " << seat_no << endl;
        cout << "Customer#? --> " << customer_id << endl;*/
        getSeat(seller_id, customer_id, seat_no);

        // Release the mutex lock.
        pthread_mutex_unlock(&seatsMutex);
        //cout << "Released seat mutex" << endl;

        // Critical region: remove a customer from a queue
        // This is FCFS, service the customer in the front
        // Group by the type of seller
        
        waitQueue.pop_front();
        //cout << "customer#" << leaveQueue.back() << " left" << endl;
        //cout << "Clean the queue" << endl;

        // Release the mutex lock before go to sleep
        // Otherwise, other sellers cannot take customers
        pthread_mutex_unlock(&positionsMutex);
        //cout << "released pos mutex" << endl;

        // Seller sells ticket in appropriate random time
        sleep(rand()%MAX_SELLING_DURATION_H + 1);

        num_custm_served[seller_id]++;

        /*sem_post(&lineUp);
        cout << "Released semaphore" << endl;*/

        //cout << "Seller finishes with customer #" << customer_id << endl;
        sprintf(event, "Customer#%d got ticket: row#%d, col#%d and leaves", customer_id, seat_no / 10, seat_no % 10);
        print(event);
        customer_id = 0;
    }
}

// The professor thread
// Taking a pointer to void as a parameter and returning a pointer to void
void *seller(void *arguments)
{
    arg_struct args = *((arg_struct *) arguments);

    /*int seller_id = *((int *) param1);
    int N = *((int *) param2);*/
    //cout << "Seller #" << seller_id << " starts its thread" << endl;

    int seller_id = args.sellerIds;
    int N = args.cusNum;

    // Time starts when seller opens their windows 
    time(&startTime);
    

    // Data Type: struct itimerval
    /*struct itimerval {
      struct timeval it_interval;  // next value 
      struct timeval it_value;     // current value 
    };*/
    /*typedef struct timeval {
      long tv_sec;  // Time interval, in seconds
      long tv_usec; // Time interval, in microseconds    
    } timeval;*/

    // Set timeval !!wall-clock time!! for an hour duration
    sellingTimer.it_value.tv_sec = TIME_DURATION;


    // The setitimer function sets the timer specified by which according to new.
    /***Function: int setitimer (int which, const struct itimerval *new, struct itimerval *old)***/
    // which: the timer code, specifying which timer to set.
    // The timer code is ITIMER_REAL, the process is sent a SIGALRM signal
    // after the specified !!wall-clock time!! has elapsed (60secs)
    setitimer(ITIMER_REAL, &sellingTimer, NULL);

    // cout << "Enter sellerServerCustomer function" << endl;

    // Sell tickets until selling time is over.
    do {
        sellerServeCustomer(seller_id);
    } while (!timesUp || num_custm_served[seller_id] <= N || tickets > 0);

    cout << "Seller closes their windows" << endl;
    return NULL;
}


// Timer signal handler.
// Used with signal function
// When time's out, set timesUp to 1
void timerHandler(int signal) {
    timesUp = 1;  // Selling time is over
}

// Main.
int main(int argc, char *argv[])
{
    int N = 0;
    if (argc > 1){
        N = atoi(argv[1]);
    }
    else{
        cout<<"Please give an argument to the program.";
        return 1;
    }


    // Initialize seats
    for(int i=0; i<NUM_SEATS; i++){
        for(int j=0; j<NUM_SEATS; j++){
            seats[i][j] = "-";
        }
    }

    // Initialize the # serviced array
    for(int i=0; i<NUM_SELLERH + NUM_SELLERM + NUM_SELLERL; i++){
        num_custm_served[i] = 0;
    }
    
    // Initialize the mutexes and the semaphore.
    pthread_mutex_init(&positionsMutex, NULL);
    pthread_mutex_init(&seatsMutex, NULL);
    pthread_mutex_init(&printMutex, NULL);


    // Passing the value of one would enable the semaphore to be shared between multiple processes
    // Initialized a semaphore with a count of 0
    //sem_init(&filledSeats, 1, 0);

    srand(time(0));

    // Create the seller threads
    // Group seller id by their type
    pthread_t sellerThreadId;
    pthread_attr_t sellerAttr;
    pthread_attr_init(&sellerAttr);

    /*sellerHIds: 0 --> NUM_SELLERH - 1;
    sellerMIds: NUM_SELLERH --> NUM_SELLERH + NUM_SELLERM - 1;
    sellerLIds: NUM_SELLERH + NUM_SELLERM --> NUM_SELLERH + NUM_SELLERM + NUM_SELLERL - 1;*/
    for (int i = 0; i < NUM_SELLERH + NUM_SELLERM + NUM_SELLERL; i++) {
        struct arg_struct args;
        args.sellerIds = i;
        args.cusNum = N;
        pthread_create(&sellerThreadId, &sellerAttr, seller, &args);
    }

    // Create the customer threads
    pthread_t customerThreadId;
    pthread_attr_t customerAttr;
    pthread_attr_init(&customerAttr);
    int customerIds[N*(NUM_SELLERH + NUM_SELLERM + NUM_SELLERL)];

    // As we assign customer_id to seat_no, customer id starts at 1
    // Each seller expects N customers
    for (int i = 1; i <= N*(NUM_SELLERH + NUM_SELLERM + NUM_SELLERL); i++) {
        customerIds[i] = i;
        pthread_create(&customerThreadId, &customerAttr, customer, &customerIds[i]);
    }


    // Send alarm when the time's out
    /*cout << "Times Up" << endl;*/
    signal(SIGALRM, timerHandler);

    // Wait for the sellers to complete the office hour.
    pthread_join(sellerThreadId, NULL);
    cout << "Finishing the last thread" << endl;
    
    // Remaining waiting customers leave.
    cout << "All customers leave" << endl;
    while (waitQueue.size() > 0) {
        leaveQueue.push(waitQueue.front());
        waitQueue.pop_front();
        
        char event[80];
        sprintf(event, "Customer %d leaves",  customer_id);
        print(event);

        //cout << "Customer#" << customer_id << " leaves" << endl;
    }

    int leftCount = leaveQueueH.size() + leaveQueueM.size() + leaveQueueL.size();
    int servedCount = N*(NUM_SELLERH + NUM_SELLERM + NUM_SELLERL) - leftCount;

    cout << "All over, among " << N*(NUM_SELLERH + NUM_SELLERM + NUM_SELLERL) << " customers\n"
         << servedCount << " custoemrs got served, and " << leftCount << " customers turned away.\n\n";

    cout << "The following is the details:\n"
         << "For ticket seler H: " << leaveQueueH.size() << " customers turned away.\n"
         << "For ticket seler M: " << leaveQueueM.size() << " customers turned away.\n"
         << "For ticket seler L: " << leaveQueueL.size() << " customers turned away.\n\n\n";

    printSeatsChart();

    cout << "End of the program" << endl;

    return 0;
}
